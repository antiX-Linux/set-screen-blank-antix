��          �      �       0  '   1  $   Y     ~     �  #   �  a   �          9     ?  $   Q  #   v     �  :  �  G   �  E   %     k     �  e   �  �   �  T   �     �  A   �  g   A  H   �  +   �               	                             
                Time before screen blanks (Minutes)     Turn on/off screen blank timeout    minutes or   seconds Could not run set-screen-blank -set Could not run turn off screen blank via xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Could not turn on screen blank Error Screen Blank Time Screen blank time has been set to: 
 Screen blanking has been turned off Success Project-Id-Version: set-screen-blank ver. 2.2
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-10-25 22:09+0300
Last-Translator: Robin, 2021
Language-Team: Russian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 2.3
   Время до выключения экрана в минутах     (Де-)активировать экранную заставку    минут или же   секунды. Конфигурация экранной заставки не может быть запущена. Не удалось запустить экранную заставку с помощью команд xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Экранная заставка не может быть активирована. Ошибка Постоянная времени заставки экрана Постоянная времени до выключения экрана установлена на
 Экранная заставка была деактивирована. Изменение было внесено. 