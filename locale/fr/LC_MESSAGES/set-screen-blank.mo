��          �      �       0  '   1  $   Y     ~     �  #   �  a   �          9     ?  $   Q  #   v     �  �  �  ,   Y  ;   �     �  	   �  ,   �  g     '   n     �     �  6   �  2   �     #               	                             
                Time before screen blanks (Minutes)     Turn on/off screen blank timeout    minutes or   seconds Could not run set-screen-blank -set Could not run turn off screen blank via xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Could not turn on screen blank Error Screen Blank Time Screen blank time has been set to: 
 Screen blanking has been turned off Success Project-Id-Version: set-screen-blank ver. 2.2
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-10-25 22:09+0300
Last-Translator: Wallon Wallon, 2021
Language-Team: French (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
   Durée avant la mise en veille (Minutes)     Activation / désactivation du délai de mise en veille    minutes ou   secondes Impossible d'exécuter set-screen-blank -set Impossible d'éteindre l'écran en veille via xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Impossible d'allumer l'écran en veille Erreur Durée de la mise en veille La mise en veille de l'écran a été réglée sur : 
 La mise en veille de l'écran a été désactivée Succès 