��          �      �       0  '   1  $   Y     ~     �  #   �  a   �          9     ?  $   Q  #   v     �  �  �  {   �  )        +  	   8  ;   B  �   ~  .        4     9  M   L  #   �     �               	                             
                Time before screen blanks (Minutes)     Turn on/off screen blank timeout    minutes or   seconds Could not run set-screen-blank -set Could not run turn off screen blank via xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Could not turn on screen blank Error Screen Blank Time Screen blank time has been set to: 
 Screen blanking has been turned off Success Project-Id-Version: set-screen-blank ver. 2.2
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-10-25 22:09+0300
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2021
Language-Team: Portuguese (Brazil) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
   Mova o controle deslizante e posicione sobre o tempo de  
  atraso (em minutos) para a ativação da proteção de tela     Ativar/desativar a proteção de tela    minutos ou   segundos Não foi possível executar o comando set-screen-blank -set Não foi possível executar os comandos xset 0 0 0 s 0 0, xset -dpms, 
xset s off, xset s noexpose para desativar a proteção de tela Não foi possível ativar a proteção de tela Erro Proteção de Tela O tempo de atraso para a ativação da proteção de tela foi definido para:
 A proteção de tela foi desativada Sucesso 