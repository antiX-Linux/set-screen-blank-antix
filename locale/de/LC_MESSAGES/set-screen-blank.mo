��          �      �       0  '   1  $   Y     ~     �  #   �  a   �          9     ?  $   Q  #   v     �  �  �  ;   R  $   �     �     �  A   �  r     2   �     �     �  ?   �  (   '     P               	                             
                Time before screen blanks (Minutes)     Turn on/off screen blank timeout    minutes or   seconds Could not run set-screen-blank -set Could not run turn off screen blank via xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Could not turn on screen blank Error Screen Blank Time Screen blank time has been set to: 
 Screen blanking has been turned off Success Project-Id-Version: set-screen-blank ver. 2.2
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-10-25 22:08+0300
Last-Translator: Robin, 2021
Language-Team: German (https://www.transifex.com/antix-linux-community-contributions/teams/120110/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
   Zeitdauer bis zum Abschalten des Bildschirms in Minuten     Bildschirmschoner (de-)aktivieren   Minuten bzw.   Sekunden eingestellt. Konfiguration des Bildschirmschoners kann nicht gestartet werden. Bildschirmschoner konnte nicht mittels xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose gestartet werden. Der Bildschirmschoner kann nicht aktiviert werden. Fehler Bildschirmschonerzeitkonstante Die Zeitkonstante bis zum Abschalten des Bildschirms wurde auf
 Der Bildschirmschoner wurde deaktiviert. Anpassung ist erfolgt 