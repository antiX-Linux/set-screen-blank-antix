��          �      �       0  '   1  $   Y     ~     �  #   �  a   �          9     ?  $   Q  #   v     �  �  �  3   n  G   �     �  	   �  )      g   *  0   �     �  &   �  0   �  0   !     R               	                             
                Time before screen blanks (Minutes)     Turn on/off screen blank timeout    minutes or   seconds Could not run set-screen-blank -set Could not run turn off screen blank via xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Could not turn on screen blank Error Screen Blank Time Screen blank time has been set to: 
 Screen blanking has been turned off Success Project-Id-Version: set-screen-blank ver. 2.2
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-10-25 22:09+0300
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: Spanish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
   Tiempo antes de suspender la pantalla (Minutos)     Iniciar/Cancelar el temporizador de la auto-suspensión de pantalla    minutos o   segundos No se pudo ejecutar set-screen-blank -set No se pudo ejecutar turn off screen blank via xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose No se pudo iniciar la suspensión de la pantalla Error Tiempo para auto-suspender la pantalla El tiempo de suspensión de la pantalla será: 
 La auto-suspensión de la pantalla no iniciará. Éxito 