��          �      �       0  '   1  $   Y     ~     �  #   �  a   �          9     ?  $   Q  #   v     �  �  �  -   ]     �     �     �  ,   �  c   �      H     i     p  K   �  1   �                    	                             
                Time before screen blanks (Minutes)     Turn on/off screen blank timeout    minutes or   seconds Could not run set-screen-blank -set Could not run turn off screen blank via xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Could not turn on screen blank Error Screen Blank Time Screen blank time has been set to: 
 Screen blanking has been turned off Success Project-Id-Version: set-screen-blank ver. 2.2
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-10-25 22:09+0300
Last-Translator: Teresio Arrighi, 2021
Language-Team: Italian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Tempo prima che lo schermo si spenga (Minuti) Timeout sospensione on/off minuti o secondi Non riesco ad eseguire set-screen-blank -set Impossibile sospendere lo schermo tramite xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Impossibile accendere lo schermo Errore Tempo per la sospensione Il tempo richiesto per la sospensione dello schermo è stato impostato in:
 La sospensione dello schermo é stata disattivata Successo 