��          �      �       0  '   1  $   Y     ~     �  #   �  a   �          9     ?  $   Q  #   v     �  �  �  +   q  %   �     �  	   �  1   �  p     /   }     �     �  3   �  $        &               	                             
                Time before screen blanks (Minutes)     Turn on/off screen blank timeout    minutes or   seconds Could not run set-screen-blank -set Could not run turn off screen blank via xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Could not turn on screen blank Error Screen Blank Time Screen blank time has been set to: 
 Screen blanking has been turned off Success Project-Id-Version: set-screen-blank ver. 2.2
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-10-25 22:09+0300
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2021
Language-Team: Portuguese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
   Atraso até ficar ecrã vazio (minutos)     Lig/Deslig a função ecrã vazio    minutos ou   segundos Não foi possível executar set-screen-blank -set Não foi possível desligar a função ecrã vazio via xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Não foi possível ligar a função ecrã vazio Erro Atraso até ao ecrã vazio O atraso da função ecrã vazio foi definido em: 
 A função ecrã vazio foi desligada Êxito 